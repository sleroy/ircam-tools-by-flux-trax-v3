> - Retail Price: 399 $ /329 €
> - [Premium member](https://www.ircam.fr/innovations/abonnements-du-forum/) Price: 199,50 $ / 164,50 € - [Ask for the voucher](https://shop.ircam.fr/index.php?controller=contact)
> - Distributed by [Flux::](https://shop.flux.audio/en_US/products/ircam-trax)
> - [Technical Support](https://support.flux.audio/portal/sign_in)
> - Get you trial [Try](https://shop.flux.audio/en_US/page/trial-request-information)
> - Software available by downloading the [Flux:: Center ](https://www.flux.audio/download/)
> - iLok.com user account is required (USB Smart Key is not required)


![Flux::](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/logos-flux%2Bircam-noir.png)

## Features ##

- Transformer - Real-time Voice and Sonic Modelling Processor
- Input/Output Gain (Transformer) -60/+18 dB
- Dry/Wet mix control (Transformer) - For mixing in unprocessed signal with the processed signal.
- Mode (Transformer) - For selecting the type of algorithm used, Voice (vocals), Instrument (other instruments), Music (mixed music)
- Source (Transformer) - For setting the frequency ranges of the material to be processed, with a learn function for automatic analysis of the material.
- Target (Transformer) - Providing a range of options for the transformation of the source material.
- Remix (Transformer) - A mini-mixing console where the various components of the processed audio can be mixed before sending the signal to the output.
* Options (Transformer) - Controlling a number of parameters affecting the analysis-re-synthesis engine.
- Modulations (Transformer) - For some fun and more creative and expressive processing, two modulators are provided; one for modulating the Pitch (Transpose), and one for the Formant.
- Spectral Envelope (Transformer)- For complex remapping of the spectrum envelope, according to a freely definable curve.
- Filters (Transformer) - A two-band high/low pass filter section that can be applied on the incoming or the outgoing material.

## TRAX v3- the next generation voice and sonic processing tools ##

A suite of three ingenious processors using pioneering technology developed during the last decades by the sound analysis/synthesis team at IRCAM, with innovative signal transformation algorithms for altering sound characteristics such as; pitch, spectral timbre and duration to manipulate properties of a voice such as gender, age and breath.

Three ingenious voice and sonic processors, Transformer, Cross Synthesis and Source Filter, for altering pitch, spectral timbre and duration to manipulate properties of a voice such as gender, age and breath.

## Transformer: Real-time Voice and Sonic Modelling Processor ##

TRAX Transformer v3 is based on an augmented phase vocoder technology and a cutting edge transformation algorithm, allowing for manipulating characteristic properties of a voice such as gender, age and breath, and on any other sound; expression, formant and pitch.

These voice transformation algorithms have already been used, with great success, in international and French cinema productions. (Farinelli, Vatel, Tirésia, Les amours d'Astrée and Céladon).

## Source (Trax Transformer) ##

With the Spectral Envelope complex remapping of the actual spectrum envelope, according to a freely definable curve, can be performed.

![Trax Transformer](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/transformer_target.png)

## Target (Trax Transformer) ##

Target, provide a range of options for the transformation of the source material. In voice mode, a range of vocal types/ranges are available, and on top of that, parameters such as gender, age and breath can be used as well. For all types of material, pitch, expression and formant are provided as well.

## Spectral Envelope (Transformer) ##

Up to eight channels of simultaneous processing for multichannel surround configuration is provided. The processing can be disengaged on any of the channels leaving the material unprocessed, bypassing the original sound.

When operating on a multi-channel bus all, up to eight in total, channels are processed by default, though the processing can be disengaged for any number of channels leaving the material unprocessed and bypassing the original sound.

By doing this in combination with serial instances the material on any number of channels can be processed with individual settings.

## Cross Synthesis - Spectral Envelope Morphing Processor ##

Options (Cross Synthesis)
- Oversampling adjusts the oversampling factor
- Overlap controls how often the analysis is updated for a given window size
- Window Size determines the time-frame base used to extract time localized data in time from the incoming material
- Transient toggles transient processing on and off.
- Amplitude (Cross Synthesis) - For controlling how the output amplitude is affected by the left and right channels.
- Frequency (Cross Synthesis) - Controls the frequency content of the output with respect to the inputs.

TRAX Cross Synthesis v3 utilizes a phase vocoder (the amplitude and the frequency/phase spectra), to morph the spectral characteristics of two sounds. The amplitude and frequency/phase spectra can be blended continuously and since the features that are used here are strongly nonlinear, the sound morphing is nonlinear as well, offering a way to create a wide range of new exciting and unusual sound effects.

![T.R.A.X. v3](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/ircam_trax-v3-full.jpg)

## Source Filter - Enhanced Sound Filtering ##

Options (Source Filter)
- Oversampling adjusts the oversampling factor
- Overlap controls how often the analysis is updated for a given window size
- Window Size determines the time-frame base used to extract time localized data in time from the incoming material.

Temporal Envelope (Source Filter) 
- Controls how much of the dynamics from channel 2 versus channel 1  will be imprinted onto the output signal.

Spectral Envelope (Source Filter) 
- Source and Filter track extraction and analysis.

True bypass control routing the incoming signal direct to the output for a smooth transition between clean and processed signal

Open Sound Control Support

TRAX Source Filter v3 is based on a signal model decomposing the signal into a time envelope describing the energy/loudness contour of the sound, and a spectral envelope describing the spectral colour of the sound timbre. The energy contour and the spectral colour of the source sound, can be continuously blended with the spectral colour and energy contour of an arbitrary filtrating sound, allowing transformation that extend well beyond the more common source filter morphing effect.

## Dual Preset Slots and Parameter Morphing ##

- Preset/Parameter slots
- To enhance the workflow the two Preset/Parameter slots, A and B, can be loaded with two full set of parameters at the same time. Apart from saving each preset, a "Global Preset" containing both the A and B settings, and the position of the "Morphing Slider", can be saved.
- Parameter Morphing Slider with Automation
- The Morphing Slider provides morphing between the parameter settings of slot A/B allowing for really creative and useful real-time tweaking. Enabling the Automation control button exposes the Morphing Slider to the host automation.

In Trax Transformer, the built in preset manager and the preset morphing slider, provides instant and intuitive control of all parameters and controls. In a second, with a simple one-click operation, everything is copied from one of the two preset slots to the other, even during playback.

## Compatibility & plugin formats ##

- **Windows** - 7 SP1, 8.1 and 10, all in 64 bits only
-  **Mac** OS X (Intel) - All versions from 10.7.5 (64bit only)
- **Ircam Trax v3 is available in the following configurations:** AudioUnit (64bit) / VST 2 (64 bit) / VS3 Native+Masscore (Pyramix 10-12) / AAX Native (64bit) / Ambisonic 1-2-3rd order - dolby atmos stem format
- **Max sample rate (depending on HOST):** 384 KHz
- **Maximum number of channels:** 16

## Hardware Specification ##

A graphic card fully supporting OpenGL 2.0 is required.

- **Windows:** If your computer has an ATi or NVidia graphics card, please assure the latest graphic drivers from the ATi or NVidia website are installed.
- **Mac OS X:** OpenGL 2.0 required - Mac Pro 1.1 & Mac Pro 2.1 are not supported.

## Software license requirements ##

In order to use the software, an iLok.com user account is required (the iLok USB Smart Key is not required).

VS3 license not included in Flux:: standard license. Additional VS3 license for Pyramix & Ovation Native/MassCore 32/64 bit versions available from [Merging Technologies](https://www.merging.com/sales).

> Tutorial
>
> - [IRCAM TRAX - Voice modification and sound creation tool by Jean-Loup Pecquais](https://youtu.be/Zyq7wIglFbs) [(version française)](https://youtu.be/5HCxW55hV2c )
